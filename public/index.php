<?php
session_start();
require('../connection.php');
require('../crud.php');
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="./style.css" rel="stylesheet">
    <title>Cafe Manager</title>
  </head>
  <body>
    <header class="primarycolor">
      <img src="https://simplonline-v3-prod.s3.eu-west-3.amazonaws.com/media/image/png/9ab5cd96-16ae-488d-803d-bf63779c5c2b.png" alt="https://simplonline-v3-prod.s3.eu-west-3.amazonaws.com/media/image/png/9ab5cd96-16ae-488d-803d-bf63779c5c2b.png" width="80" height="80">
      <h1>Gestion du Café</h1>
    </header>
    <main>
      <?php
      if (isset($_GET['_'])) {
        require('../firstconnection.php');
      } else {
        require('../login.php');
      }
      ?>
    </main>
    <footer class="primarycolor">
      <h5>By Rémi</h5>
    </footer>
  </body>
</html>

<!-- ?_=c05ybTJWeGZtNk1SQU92YlVRMkxTNmxKNzM2NEZ4UjBoYU1aYTNDZ0RVRT0= -->