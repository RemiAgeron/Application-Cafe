<?php function API($conn, $request) {

}

function CRUD($conn, $post) {
  return $post['action']($conn, $post);
}

function GetAll($conn) {
  $sql = "SELECT * FROM `user` ORDER BY `drink` DESC";
  return $conn->query($sql);
}

function GetDone($conn) {
  $sql = "SELECT * FROM `payday` WHERE `done` = 0 AND `deleted` = 0";
  return $conn->query($sql);
}

function GetDrinkers($conn) {
  $sql = "SELECT * FROM `user` WHERE `drink` = 1 ORDER BY `paid`";
  return $conn->query($sql);
}

function GetOne($conn, $post) {
  $sql = "SELECT * FROM `user` WHERE `name` = \"$post[name]\"";
  return $conn->query($sql);
}

function CheckDone($conn) {
  $sql = "SELECT * FROM `user` WHERE `paid` = 0";
  return $conn->query($sql);
}

function CheckPass($conn, $username, $password) {
  $sql = "SELECT * FROM `admin` WHERE `username` = \"$username\" AND `password` = \"$password\"";
  return $conn->query($sql);
}

function Ajouter($conn, $post) {
  $sql = "SELECT * FROM `user` WHERE `name` = \"$post[name]\"";
  if ($conn->query($sql)->num_rows == 0) {
    $sql = "INSERT INTO `user` (`name`) VALUES(\"$post[name]\")";
    return $conn->query($sql);
  } else {
    $sql = "UPDATE `user` SET `deleted` = 0 WHERE `name` = \"$post[name]\"";
    return $conn->query($sql);
  }
}

function PayDay($conn, $price) {
  $sql = "INSERT INTO `payday` (`date`,`price`) VALUES (CURRENT_TIMESTAMP, \"$price\")";
  return $conn->query($sql);
}

function Payer($conn, $post) {
  $paid = GetOne($conn, $post)->fetch_array()['paid'] == 0 ? 1 : 0;
  $sql = "UPDATE `user` SET `paid` = \"$paid\" WHERE `name` = \"$post[name]\"";
  return $conn->query($sql);
}

function ResetPaid($conn) {
  $sql = "UPDATE `user` SET `paid` = 0 WHERE `drink` = 1";
  return $conn->query($sql);
}

function Boit($conn, $post) {
  $drink = GetOne($conn, $post)->fetch_array()['drink'] == 0 ? 1 : 0;
  $paid = GetOne($conn, $post)->fetch_array()['drink'] == 0 ? 0 : 1;
  $sql = "UPDATE `user` SET `drink` = \"$drink\", `paid` = \"$paid\" WHERE `name` = \"$post[name]\"";
  return $conn->query($sql);
}

function Done($conn) {
  $sql = "UPDATE `payday` SET `done` = 1, `deleted` = 1  WHERE `done` = 0 AND `deleted` = 0 ORDER BY `date` LIMIT 1";
  return $conn->query($sql);
}

function NewPassword($conn, $username, $password, $lastpassword) {
  $sql = "UPDATE `admin` SET `password` = \"$password\"  WHERE `username` = \"$username\" AND `password` = \"$lastpassword\"";
  return $conn->query($sql);
}

function Supprimer($conn, $post) {
  $sql = "UPDATE user SET `deleted` = 1 WHERE `name` = \"$post[name]\"";
  return $conn->query($sql);
}

function CancelPayday($conn) {
  $sql = "UPDATE payday SET `deleted` = 1  WHERE `deleted` = 0";
  return $conn->query($sql);
}