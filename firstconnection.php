<?php
$data = explode(" ", openssl_decrypt(base64_decode($_GET['_']), "AES-128-ECB", "checkin"));

if (CheckPass($conn, $data[0], $data[1])->num_rows == 1) {
  $_SESSION['advice'] = "Vous devriez changer de mot de passe";
  $_SESSION['user'] = $data[0];
  $_SESSION['password'] = $data[1];
  header("Location: .");
}