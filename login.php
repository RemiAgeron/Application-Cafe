<?php if (isset($_POST['username']) && isset($_POST['password'])) {

  if (CheckPass(
    $conn,
    $_POST['username'],
    openssl_encrypt($_POST['password'], "AES-128-ECB", $_POST['username'])
  )->num_rows == 1) {

    $_SESSION['user'] = $_POST['username'];
    header("Location: .");

  } else {
    ?> <p style="color: red"> <?= "Connexion refusée" ?> </p> <?php
  }

}

if (isset($_SESSION['user'])) {

  if (isset($_SESSION['advice'])) {
    
    ?> <p style="color: red"> <?= $_SESSION['advice'] ?> </p> <?php
  
  } ?>

  <form action="." method="post">
    <input type="submit" name="logout" value="Déconnexion">
    <label class="user"><?= $_SESSION['user'] ?></label>
    <input type="submit" name="change" value="Changer le mot de passe">
  </form>

  <?php if (isset($_POST['change'])) { ?>
    
    <form action="." method="post">

      <?php
      if (isset($_SESSION['advice'])) { ?>
        <input type="hidden" name="lastpassword" value=<?= openssl_decrypt($_SESSION['password'], "AES-128-ECB", $_SESSION['user']) ?>>
      <?php } else { ?>
        <input type="password" name="lastpassword" placeholder="Ancien mot de passe">
      <?php } ?>

      <input type="password" name="newpassword" placeholder="Nouveau mot de passe">
      <input type="password" name="confpassword" placeholder="Confirmation du mot de passe">
      <input type="submit" name="change" value="Confirmer">
    </form>

    <?php if (
      $_POST['change'] == "Confirmer"
      && isset($_POST['lastpassword'])
      && isset($_POST['newpassword'])
      && isset($_POST['confpassword'])) {

      $uppercase = preg_match('@[A-Z]@', $_POST['newpassword']);
      $lowercase = preg_match('@[a-z]@', $_POST['newpassword']);
      $number = preg_match('@[0-9]@', $_POST['newpassword']);
      $length = strlen($_POST['newpassword']);

      if($_POST['newpassword'] == $_POST['confpassword'] && $uppercase && $lowercase && $number && $length >= 6) {

        if (CheckPass(
          $conn,
          $_SESSION['user'],
          openssl_encrypt($_POST['lastpassword'], "AES-128-ECB", $_SESSION['user'])
        )->num_rows == 1) {

          NewPassword($conn, $_SESSION['user'], openssl_encrypt($_POST['newpassword'], "AES-128-ECB", $_SESSION['user']), openssl_encrypt($_POST['lastpassword'], "AES-128-ECB", $_SESSION['user']));
          session_unset();
          header("Location: .");

        } else {
          ?> <p style="color: red"> <?= "Mot de passe incorrect" ?> </p> <?php
        }

      } else {
        ?> <p style="color: red">Mot de passe non-conforme</br>minimum 6 caractères, au moins un chiffre,</br>une lettre majuscule et une minuscule</br>Ou confirmation du mot de passe incorrect</p> <?php
      }

    }

  } else {
    require('../new_drinker.php');
    require('../manage_drinker.php');
    require('../payday.php');
  }

  if (isset($_POST['logout'])) {
    session_unset();
    header("Location: .");
  }

} else { ?>

  <form class="miniform" action="." method="post">
    <input type="text" name="username" placeholder="pseudonyme">
    <input type="password" name="password" placeholder="mot de passe">
    <input class="marg10vert" type="submit" value="Connexion">
  </form>

<?php } ?>