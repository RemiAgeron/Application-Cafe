<?php if (isset($_POST['Calcul']) && isset($_POST['bill'])) {

  if ($_POST['bill'] > 0) {

    ResetPaid($conn);

    if (GetDone($conn)->num_rows == 0) {
      PayDay($conn, intval(round($_POST['bill'] * 100)));
      header("Location: .");
    }


  } else { ?>

  <p style="color: red"> <?= "Veuillez vérifier la somme et réessayer" ?> </p>

  <?php }

}

if (isset($_POST['cancel'])) {
  CancelPayday($conn);
  header("Location: .");
}

if (isset($_POST['action'])) {
  if (CheckDone($conn)->num_rows == 0) {
    Done($conn);
    header("Location: .");
  }
}

$done = GetDone($conn);

if ($done->num_rows != 0) {

  $res = round((($done->fetch_array()['price'] / 100) / GetDrinkers($conn)->num_rows) * 100) / 100;
  echo "Chaque personne doit donner : " . $res . "€"; ?>

  <ul style="list-style-type: none; padding: 0;">

    <?php $data = GetDrinkers($conn);
    foreach ($data as $item) { ?>

      <li>
        <form action="." method="post">
          <input type="hidden" name="name" value="<?= $item['name'] ?>">
          <input <?php if($item['paid'] == 0) echo 'style="color:red"'?> type="submit" name="action" value="Payer">
          <?= $item['name'] ?>
        </form>
      </li>

    <?php } ?>

  </ul>

  <form action="." method="post">
    <input type="submit" name="cancel" value="Annuler">
  </form>

<?php } else { ?>

  <form action="." method="post">
    <input type="number" name="bill" placeholder="Entrez le prix du café ici">
    <input type="submit" name="Calcul" value="Calcul">
  </form>

<?php }