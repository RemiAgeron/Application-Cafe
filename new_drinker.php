<?php if(isset($_POST['action'])) {
  if($_POST['action'] == "Ajouter") {
    if(preg_match('/^[\p{L}-]*$/u', $_POST['name']) == 1) {
      crud($conn, $_POST);
    } else {
      ?> <p style="color: red"> <?= "Seulement des lettres et sans espaces !" ?> </p> <?php
    }
  } else {
    crud($conn, $_POST);
  }
}

if (GetDone($conn)->num_rows == 0) { ?>

  <form action="." method="post">
    <input type="text" name="name" placeholder="Entrez le nom ici">
    <input type="submit" name="action" value="Ajouter">
  </form>

<?php } ?>